let yup = require("yup");
let request = require('request');
let ModelActor = require("../models/ModelActors");
let ModelUser   = require('../models/ModelUser');

let schema_post = yup.object().shape({
	nama_aktor:yup.string().required().max(255),
	jenis_kelamin:yup.string().required().oneOf(["L","P"]),
	tempat_lahir:yup.string().required().max(255),
	tanggal_lahir:yup.date().required(),
	tahun_debut:yup.number().integer().max(2999).min(1000).required(),
	kewarganegaraan:yup.string().required().max(255),
	jumlah_anak:yup.number().integer().required().max(9),
	tinggi_badan:yup.number().required().max(300),
	url_foto:yup.string().url().required()
});

let schema_update = yup.object().shape({
	nama_aktor:yup.string().max(255),
	jenis_kelamin:yup.string().oneOf(["L","P"]),
	tempat_lahir:yup.string().max(255),
	tanggal_lahir:yup.date(),
	tahun_debut:yup.number().integer().max(2999).min(1000),
	kewarganegaraan:yup.string().max(255),
	jumlah_anak:yup.number().integer().max(9),
	tinggi_badan:yup.number().max(300),
	url_foto:yup.string().url()
});

class ControllerActor {
	async listActorDgID_AKTOR(req,res,next){
		let cek_connection = await ModelUser.getConnection();
		if (ModelUser.connectionError(cek_connection.status)) return res.status(cek_connection.status).send(cek_connection.result);
		
		let id = req.params.ID_AKTOR;
		let result = await ModelActor.select_actor(id);

		if (result.length > 0){
			const isi = result[0];
			let format_bulan = ['','Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'November', 'Desember'];
			let tanggal_lahir = (new Date(isi.TANGGAL_LAHIR).getDate())+' '+format_bulan[(new Date(isi.TANGGAL_LAHIR).getMonth())]+' '+(new Date(isi.TANGGAL_LAHIR).getFullYear());
			
			let news = null;
			try{
				news = await request.get("https://newsapi.org/v2/everything?apiKey=*************api-key************&q="+isi.NAMA_AKTOR);
			}catch(err){
				result[0] = {ID:isi.ID_AKTOR,Name:isi.NAMA_AKTOR,"Date of Birth":tanggal_lahir,Height:isi.TINGGI_BADAN,Citizenship:isi.KEWARGANEGARAAN};
				return res.status(200).send(result[0]);
			}

			result[0] = {ID:isi.ID_AKTOR,Name:isi.NAMA_AKTOR,"Date of Birth":tanggal_lahir,Height:isi.TINGGI_BADAN,Citizenship:isi.KEWARGANEGARAAN,News:JSON.parse(news.body).articles};
			res.status(200).send(result[0]);
		}

		else res.status(400).send('Not Found');
	}
	async listActordgPencarian(req,res,next){
		let cek_connection = await ModelUser.getConnection();
		if (ModelUser.connectionError(cek_connection.status)) return res.status(cek_connection.status).send(cek_connection.result);
		
		let data = {
			nama_aktor: req.query.nama_aktor,
			jenis_kelamin: req.query.jenis_kelamin,
			kewarganegaraan: req.query.kewarganegaraan,
			tinggi_badan: req.query.tinggi_badan,
			paging:req.query.paging
		};
		
		let result = await ModelActor.select_actor_by_name_citizenship_gender_height(data);

		let format_bulan = ['','Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'November', 'Desember'];
		for(let i=0;i<result.length;i++){
			let isi = result[i];
			let tanggal_lahir = (new Date(isi.TANGGAL_LAHIR).getDate())+' '+format_bulan[(new Date(isi.TANGGAL_LAHIR).getMonth())]+' '+(new Date(isi.TANGGAL_LAHIR).getFullYear());

			result[i] = {
				ID:result[i].ID_AKTOR,
				Name:result[i].NAMA_AKTOR,
				"Date of Birth":tanggal_lahir,
				Gender:result[i].JENIS_KELAMIN,
				Citizenship:result[i].KEWARGANEGARAAN,
				Picture:result[i].URL_FOTO
			};
		}

		if (result.length > 0) res.status(200).send(result);
		else res.status(404).send('Not Found');
	}
	async insertCast(req,res,next){
		let result = await ModelActor.insert_cast(req.params.id_aktor,req.body.id_movie);
		res.status(result.status).send(result.result);
	}
	async insertActor(req,res,next){
		try {
			req.body = await schema_post.validate(req.body,{abortEarly:false})
		}catch(error){
			console.log(error);
			return res.status(400).send(error.errors.join('\n'));   
		}
		
		let result = await ModelActor.insert_actor(req.body);
		res.status(result.status).send(result.result);
	}
	async updateActor(req,res,next){
		try {
			req.body = await schema_update.validate(req.body,{abortEarly:false})
		}catch(error){
			console.log(error);
			return res.status(400).send(error.errors.join('\n'));   
		}
		req.body.id_aktor = req.params.id_aktor;
		let result = await ModelActor.update_actor(req.body);
		res.status(result.status).send(result);
	}
	async deleteActor(req,res,next){
		let result = await ModelActor.delete_actor(req.params.id_aktor);
		return res.status(result.status).send(result.result);
	}
	async deleteCast(req,res,next){
		let result = await ModelActor.delete_cast(req.params.id_aktor,req.params.id_movie);
		return res.status(result.status).send(result.result);
	}
}
module.exports = new ControllerActor();