let yup = require("yup");
let ModelUser   = require('../models/ModelUser');
let ModelMovies = require("../models/ModelMovies");
let ModelActor  = require("../models/ModelActors");
let request = require("request");
let util = require("util");

request.get = util.promisify(request.get);

let schema_post = yup.object().shape({
	nama_movies:yup.string().required().max(255)
});

class ControllerMovies {
	async listMovies (req,res,next){
		let cek_connection = await ModelUser.getConnection();
		if (ModelUser.connectionError(cek_connection.status)) return res.status(cek_connection.status).send(cek_connection.result);
		
			let judul = await ModelMovies.select_nama_movies(req.query.nama_movies);
			if (!judul) return res.status(404).send('Not Found');
		
		let data_movies = null
		try {
			data_movies = await request.get("http://www.omdbapi.com/?apikey=********&t="+judul);
		} catch (error) {
			return res.status(500).send('Internal Server Error');
		}
		
		data_movies.body = JSON.parse(data_movies.body);
		
		let aktor = await ModelActor.select_actor_by_moviename(data_movies.body.Title);
		
		for(let i=0;i<aktor.length;i++){
			aktor[i] = {ID:aktor[i].ID_AKTOR,Name:aktor[i].NAMA_AKTOR,Gender:aktor[i].JENIS_KELAMIN,Citizenship:aktor[i].KEWARGANEGARAAN,Picture:aktor[i].URL_FOTO};
		}

		let tampilkan_data = {
			Title : data_movies.body.Title,
			Year : data_movies.body.Year,
			Released : data_movies.body.Released,
			Genre : data_movies.body.Genre,
			Actors : aktor
		}/** */

		// return res.status(200).send(tampilkan_data);
		if (tampilkan_data.Actors.length > 0) res.status(200).send(tampilkan_data);
		else res.status(404).send('Not Found');
	}
	async insertMovies(req,res,next){
		try {
			req.body = await schema_post.validate(req.body,{abortEarly:false});
		}catch(error){
			return res.status(400).send(error.errors.join('\n'));   
		}
		let data_movie = null
		try {
			data_movie = await request.get("http://www.omdbapi.com/?apikey=********&t="+req.body.nama_movies);
		}catch(error){
			return res.status(500).send('Internal Server Error');   
		}
		data_movie.body = JSON.parse(data_movie.body);
		
		if(req.body.nama_movies != data_movie.body.Title){
			return res.status(404).send("Not Found");
		}
		
		let result = await ModelMovies.insert_movies(req.body.nama_movies);
		return res.status(result.status).send(result.result);
	}
	async updateMovies(req,res,next){
		try {
			req.body = await schema_post.validate(req.body,{abortEarly:false})
		}catch(error){
			return res.status(400).send(error.errors.join('\n'));   
		}
		
		let id_movies = req.params.id_movies;
		let nama_movies = req.body.nama_movies;
		let result = await ModelMovies.update_movies(id_movies,nama_movies);
		return res.status(result.status).send(result.result);
	}
	async deleteMovies(req,res,next){
		let id_movies = req.params.id_movies;
		let result = await ModelMovies.delete_movies(id_movies);
		res.status(result.status).send(result.result);
	}
}

module.exports = new ControllerMovies();