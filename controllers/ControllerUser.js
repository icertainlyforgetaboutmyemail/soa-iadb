/* Load Bcrypt untuk enkripsi */
const bcrypt = require("bcrypt");
/* Load untuk api key */
const jwt = require("jsonwebtoken");
/* Load untuk pengecekan */
let yup = require("yup");

/* Load Model (Codingan database) */
const modelUser = require('../models/ModelUser');

/* Set Schema */
let schema_register = yup.object().shape({
	email_user:yup.string().email().required().max(255),
	nama_user:yup.string().required().max(255),
	password_user:yup.string().required().max(255),
});

let schema_login = yup.object().shape({
	email_user:yup.string().email().required().max(255),
	password_user:yup.string().required().max(255),
});
/* End Schema */

class ControllerUser
{
    /* Untuk authentication key */
	auth(req,res,next,level){
		let token = req.query.api_key;
		let user = {};
		try
		{
			user = jwt.verify(token,'IADB');
		}catch(error){
			return res.status(40).send("Unauthorized");
		}
		
		console.log("Authorization : ");
		console.log(user);
		
		if (user.level==level || user.level=="A") next();
		else return res.status(403).send("Forbidden");
	}
	
    /* Untuk api key */ 
    token(email_user,jenis_user){
        return jwt.sign({
            "email":email_user,
            "level":jenis_user
        },'IADB');
    }

    /* Untuk login */
    async login(req,res,next){      
        try {
            /* Cek data user yang terkirim */
            let dataUser = await schema_login.validate(req.body,{abortEarly:false})

            /* Kirim data ke Model Database */
            let sql = await modelUser.login(
                dataUser.email_user,
                dataUser.password_user
            );

            /* Bila status code 201 (Created) munculkan data */
            if (sql.status == 200) res.status(sql.status).send(this.token(dataUser.email_user,sql.result[0].JENIS_USER));

            /* Bila status code 400 (Bad Request) munculkan Error tanpa datanya */
            else res.status(sql.status).send(sql.result);
        } catch (error) {
            /* Bila status code 400 (Bad Request) munculkan Error beserta datanya */
            // res.status(400).send(error); // Bila terjadi error yang tidak diketahui unremark line ini
            res.status(400).send(error.errors.join('\n'));   
        }
        /** */
    }

    async register(req,res,next){
        try {
            /* Cek data user yang terkirim */
            let dataUser = await schema_register.validate(req.body,{abortEarly:false});

            /* Encrypt password user */
            let salt = await bcrypt.genSalt(10);
            let hashed = await bcrypt.hash(dataUser.password_user, salt);

            /* Kirim data ke Model Database */
            let sql = await modelUser.register(
                dataUser.email_user,
                dataUser.nama_user,
                hashed
            );

            /* Bila status code 201 (Created) munculkan data */
            if (sql.status == 201) res.status(sql.status).send(this.token(dataUser.email_user,'U'));

            /* Bila status code 400 (Bad Request) munculkan Error beserta datanya */            
            else res.status(sql.status).send(sql.result);

        } catch (error) {
            /* Bila status code 400 (Bad Request) munculkan Error beserta datanya */
            // res.status(400).send(error); // Bila terjadi error yang tidak diketahui unremark line ini
            res.status(400).send(error.errors.join('\n'));   
        }
    }
}

module.exports = new ControllerUser();
