-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2019 at 06:34 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktor`
--

CREATE TABLE `aktor` (
  `ID_AKTOR` varchar(5) NOT NULL,
  `NAMA_AKTOR` varchar(255) NOT NULL,
  `JENIS_KELAMIN` varchar(1) NOT NULL,
  `TEMPAT` varchar(255) NOT NULL,
  `TANGGAL_LAHIR` date NOT NULL,
  `TAHUN_DEBUT` int(11) NOT NULL,
  `KEWARGANEGARAAN` varchar(255) NOT NULL,
  `JUMLAH_ANAK` int(11) NOT NULL,
  `TINGGI_BADAN` int(11) NOT NULL,
  `URL_FOTO` text NOT NULL,
  `STATUS` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `ID_MOVIES` varchar(5) NOT NULL,
  `NAMA_MOVIES` varchar(255) NOT NULL,
  `STATUS` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

-- INSERT INTO `movies` (`ID_MOVIES`, `NAMA_MOVIES`) VALUES
-- ('M1801', 'Mission: Impossible – Fallout'),
-- ('M1802', 'Annihilation'),
-- ('M1803', 'BlacKkKlansman'),
-- ('M1804', 'Hereditary'),
-- ('M1805', 'Roma'),
-- ('M1806', 'The Favourite'),
-- ('M1807', 'A Star Is Born');

-- --------------------------------------------------------

--
-- Table structure for table `peran`
--

CREATE TABLE `peran` (
  `ID_AKTOR` varchar(5) NOT NULL,
  `ID_MOVIES` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `EMAIL_USER` varchar(255) NOT NULL,
  `NAMA_USER` varchar(255) NOT NULL,
  `PASSWORD_USER` varchar(255) NOT NULL,
  `JENIS_USER` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

-- INSERT INTO `user` (`EMAIL_USER`, `NAMA_USER`, `PASSWORD_USER`, `JENIS_USER`) VALUES
-- ('erick@email.com', 'erick', '$2b$10$mdPUglPRuWy1cYMnAnrK7u99BjVEAREWl94nuV7uLtZtr6QrQzd9a', 'U');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktor`
--
ALTER TABLE `aktor`
  ADD PRIMARY KEY (`ID_AKTOR`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`ID_MOVIES`);

--
-- Indexes for table `peran`
--
ALTER TABLE `peran`
  ADD PRIMARY KEY (`ID_AKTOR`,`ID_MOVIES`),
  ADD KEY `FK_PERAN2` (`ID_MOVIES`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`EMAIL_USER`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `peran`
--
ALTER TABLE `peran`
  ADD CONSTRAINT `FK_PERAN` FOREIGN KEY (`ID_AKTOR`) REFERENCES `aktor` (`ID_AKTOR`),
  ADD CONSTRAINT `FK_PERAN2` FOREIGN KEY (`ID_MOVIES`) REFERENCES `movies` (`ID_MOVIES`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
