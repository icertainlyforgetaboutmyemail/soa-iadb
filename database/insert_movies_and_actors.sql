INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0001','Avenger','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0002','Avengers: Infinity War','A');

INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0003','batman vs superman','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0004','justice league','A');

INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0005','x-men Apocalypse','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0006','X-Men First Class','A');

INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0007','Fantastic Four','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0008','Fantastic 4 Rise of the Silver Surfer','A');

INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0009','Harry Potter and the Chamber of Secrets','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0010','Harry Potter and the Prisoner of Azkaban','A');

INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0011','The Chronicles of Narnia The Voyage of the Dawn Treader','A');
INSERT INTO `movies`(`ID_MOVIES`, `NAMA_MOVIES`,`STATUS`) VALUES ('M0012','The Chronicles of Narnia Prince Caspian','A');

INSERT INTO `aktor`(`ID_AKTOR`, `NAMA_AKTOR`, `JENIS_KELAMIN`, `TEMPAT`, `TANGGAL_LAHIR`, `TAHUN_DEBUT`, `KEWARGANEGARAAN`, `JUMLAH_ANAK`, `TINGGI_BADAN`, `URL_FOTO`, `STATUS`) VALUES 
('A0001','Robert Downey Jr.','L','Manhattan','1965-4-4','','US','','174','http://www.gstatic.com/tv/thumb/persons/67369/67369_v9_bb.jpg','A'),
('A0002','Chris Evans','L','Boston','1981-6-13','','US','','183','http://www.gstatic.com/tv/thumb/persons/173489/173489_v9_ba.jpg','A'),
('A0003','Chris Hemsworth','L','Melbourne','1983-8-11','','Australia','','190','http://www.gstatic.com/tv/thumb/persons/528854/528854_v9_bb.jpg','A'),
('A0004','Mark Ruffalo','L','Kenosha','1967-11-22','','US','','173','http://www.gstatic.com/tv/thumb/persons/43389/43389_v9_bb.jpg','A'),

('A0005','Ben Affleck','L','California','1972-8-15','','US','','192','http://www.gstatic.com/tv/thumb/persons/7530/7530_v9_bb.jpg','A'),
('A0006','Henry Cavill','L','Saint Helier','1983-5-5','','England','','185','https://m.media-amazon.com/images/M/MV5BMTUxNTExMzUzOF5BMl5BanBnXkFtZTgwOTI1MjA3OTE@._V1_.jpg','A'),
('A0007','Amy Adams','P','Caserma Ederle','1974-8-20','','Italy','','163','http://www.gstatic.com/tv/thumb/persons/168053/168053_v9_bb.jpg','A'),
('A0008','Gal Gadot','P','Petah Tikva','1985-4-30','','Israel','','178','http://www.gstatic.com/tv/thumb/persons/532761/532761_v9_bb.jpg','A'),

('A0009','James McAvoy','L','Glasgow','1979-4-21','','UK','','170','http://www.gstatic.com/tv/thumb/persons/253919/253919_v9_bb.jpg','A'),
('A0010','Michael Fassbender','L','Heidelberg','1977-4-2','','Germany','','183','http://www.gstatic.com/tv/thumb/persons/305537/305537_v9_ba.jpg','A'),
('A0011','Jennifer Lawrence','P','Indian Hills','1990-8-15','','US','','175','http://www.gstatic.com/tv/thumb/persons/389416/389416_v9_bc.jpg'),
('A0012','Nicholas Hoult','L','Wokingham','1989-12-7','','UK','','190','http://www.gstatic.com/tv/thumb/persons/259192/259192_v9_ba.jpg','A'),

('A0013','Ioan Gruffudd','L','Llwydcoed','1973-10-6','','UK','','182','http://www.gstatic.com/tv/thumb/persons/159152/159152_v9_ba.jpg','A'),
('A0014','Jessica Alba','P','Pomona','1981-4-28','','US','','169','http://www.gstatic.com/tv/thumb/persons/165531/165531_v9_ba.jpg','A'),
('A0015','Michael Chiklis','L','Lowell, Massachusetts','1963-8-30','','UK','','175','http://www.gstatic.com/tv/thumb/persons/317/317_v9_bb.jpg','A'),

('A0016','Daniel Radcliffe','L','London','1989-7-23','','UK','','165','http://www.gstatic.com/tv/thumb/persons/233401/233401_v9_ba.jpg','A'),
('A0017','Rupert Grint','L','Harlow','1988-8-24','','UK','','173','http://www.gstatic.com/tv/thumb/persons/247025/247025_v9_bc.jpg','A'),
('A0018','Emma Watson','P','Paris','1990-4-15','','France','','165','http://www.gstatic.com/tv/thumb/persons/247026/247026_v9_bb.jpg','A'),

('A0019','Anna Popplewell','P','London','1988-12-16','','UK','','161','http://www.gstatic.com/tv/thumb/persons/212818/212818_v9_bb.jpg','A'),
('A0020','Georgie Henley','P','Ilkley','1995-7-9','','UK','','163','http://www.gstatic.com/tv/thumb/persons/485926/485926_v9_bb.jpg','A'),
('A0021','Skandar Keynes','L','London','1991-9-5','','UK','','173','http://www.gstatic.com/tv/thumb/persons/485927/485927_v9_ba.jpg','A'),
('A0022','William Moseley','L','Sheepscombe','1987-4-27','','UK','','178','http://www.gstatic.com/tv/thumb/persons/259966/259966_v9_bb.jpg','A');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0001','M0001');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0002','M0001');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0003','M0001');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0004','M0001');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0001','M0002');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0002','M0002');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0003','M0002');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0004','M0002');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0005','M0003');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0006','M0003');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0007','M0003');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0008','M0003');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0005','M0004');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0006','M0004');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0007','M0004');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0008','M0004');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0009','M0005');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0010','M0005');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0011','M0005');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0012','M0005');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0009','M0006');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0010','M0006');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0011','M0006');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0012','M0006');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0013','M0007');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0014','M0007');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0015','M0007');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0013','M0008');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0014','M0008');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0015','M0008');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0016','M0009');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0017','M0009');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0018','M0009');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0016','M0010');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0017','M0010');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0018','M0010');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0019','M0011');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0020','M0011');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0021','M0011');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0022','M0011');

INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0019','M0012');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0020','M0012');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0021','M0012');
INSERT INTO `peran`(`ID_AKTOR`, `ID_MOVIES`) VALUES ('A0022','M0012');
