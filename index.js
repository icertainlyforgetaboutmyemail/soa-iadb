console.clear();
const express= require("express");
const app = express();

const user = require("./routes/User");
const movies = require("./routes/Movies");
const actor = require("./routes/Actor");

/* Agar bisa mendapatkan data yang telah terkirim secara object  */
app.use(express.json());

/**
 * Berisi register dan login
 * Link localhost:3000/api/login
 * Link localhost:3000/api/register
 */

app.use("/api",user);

/**
 * Berisi Get, Post, Put, Delete
 * Link localhost:3000/api/movies
 * Link localhost:3000/api/movies/:ID_MOVIES
 */

app.use("/api/movies",movies);

/**
 * Berisi Get, Post, Put, Delete
 * Link localhost:3000/api/actor
 * Link localhost:3000/api/actor/:ID_ACTOR
 */

app.use("/api/actor",actor);

app.listen(3000, function () {
    console.log("Listening on port 3000");
});