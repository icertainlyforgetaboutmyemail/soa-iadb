const conn = require('./db');
const modeluser = require('./ModelUser');

class ModelActors {
	async select_actor(id_aktor){
		try {
			let result = await conn.query("select * from aktor where id_aktor = ? and status = 'A'",[id_aktor]);
			return result;
		}catch(err){return err};
	}
	
	async select_actor_by_name_citizenship_gender_height(data){
		let text_query = "select * from aktor where status = 'A' ";

		let arr_query = [];

		if (data.nama_aktor != null) {
			text_query += 'and nama_aktor like ? ';
			arr_query[arr_query.length] = '%'+data.nama_aktor+'%';
		}
		if (data.jenis_kelamin != null) {
			text_query += 'and jenis_kelamin = ? ';
			arr_query[arr_query.length] = data.jenis_kelamin;
		}
		if (data.kewarganegaraan != null) {
			text_query += 'and kewarganegaraan like ? ';
			arr_query[arr_query.length] = ''+data.kewarganegaraan+'';
		}
		if (data.tinggi_badan != null) {
			text_query += 'and tinggi_badan >= ? ';
			arr_query[arr_query.length] = data.tinggi_badan;
		}

		if (data.paging != null) {
			data.paging = (data.paging)*5;
			text_query += 'limit '+data.paging+',5 ';
		}
		else {
			text_query += 'limit 0,5 ';
		}
		// console.log(text_query);
		let result = await conn.query(text_query,arr_query);

		return result;
	}

	async select_actor_by_moviename(nama){
		let result = await conn.query('select * from aktor,movies,peran where peran.id_movies=movies.id_movies and peran.id_aktor=aktor.id_aktor and nama_movies = ?',[nama]);
		return result;
	}

	async insert_cast(id_aktor, id_movie){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let result = {};
		try{
			result = await conn.query("insert into peran values(?,?)",[id_aktor,id_movie]);
		} catch(error){
            return {status:400,result:error.sqlMessage};
        }
		
		return {status:200,result:{id_aktor,id_movie}};
	}
	
	async insert_actor(data){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let movies = await conn.query("SELECT concat('A',lpad(substr(ID_AKTOR,2,4)+1,4,0)) ID_AKTOR FROM aktor WHERE 1 ORDER BY ID_AKTOR DESC LIMIT 1");
		let id_aktor = movies[0].ID_AKTOR;
		let result = "";
		let tanggal_lahir = data.tanggal_lahir.getFullYear()+"-"+data.tanggal_lahir.getMonth()+"-"+data.tanggal_lahir.getDate();
		let data_insert = [
			id_aktor,
			data.nama_aktor,
			data.jenis_kelamin,
			data.tempat_lahir,
			tanggal_lahir,
			data.tahun_debut,
			data.kewarganegaraan,
			data.jumlah_anak,
			data.tinggi_badan,
			data.url_foto,
			"A"
		];
		try{
			result = await conn.query("insert into aktor values(?,?,?,?,?,?,?,?,?,?,?)",data_insert);
		} catch(error){
            return {status:400,result:error.sqlMessage};
        }
		data.id_aktor = id_aktor;
		return {status:200,result:data};
	}
	
	async update_actor(data){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		let tanggal_lahir = data.tanggal_lahir.getFullYear()+"-"+data.tanggal_lahir.getMonth()+"-"+data.tanggal_lahir.getDate();
		let data_update = [
			data.nama_aktor,
			data.jenis_kelamin,
			data.tempat_lahir,
			tanggal_lahir,
			data.tahun_debut,
			data.kewarganegaraan,
			data.jumlah_anak,
			data.tinggi_badan,
			data.url_foto,
			data.id_aktor
		];
		
		let result = await conn.query("update aktor set "+
			"nama_aktor = ? and"+
			" jenis_kelamin  = ? and"+
			" tempat = ? and"+
			" tanggal_lahir = ? and"+
			" tahun_debut = ? and"+
			" kewarganegaraan = ? and"+
			" jumlah_anak = ? and"+
			" tinggi_badan = ? and"+
			" url_foto = ?"+
			" where id_aktor = ?",data_update);
		
		if (result.affectedRows > 0) return {status:200,result:data};
		else return {status:400,result:"No data found"};
	}
	
	async delete_actor(id_aktor){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let data = await this.select_actor(id_aktor);
		let result = await conn.query("update aktor set status='X' where id_aktor = ?",[null,id_aktor]);
		if (result.affectedRows > 0) return {status:200,result:data};
		else return {status:400,result:"No data found"};
		/** */
	}
	
	async delete_cast(id_aktor, id_movie){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let result = await conn.query("delete from peran where id_aktor = ? and id_movies = ?",[id_aktor,id_movie]);
		if (result.affectedRows > 0) return {status:200,result:{id_aktor:id_aktor,id_movie:id_movie}};
		else return {status:400,result:"No data found"};
	}
}

module.exports = new ModelActors();