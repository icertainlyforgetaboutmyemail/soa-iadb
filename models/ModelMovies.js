const conn = require('./db');
const modeluser = require('./ModelUser');
class ModelMovies {
	async select_movies (id_movies){
		let result = await conn.query("select * from movies where id_movies=? and status = 'A'",[id_movies]);
		return result;
	}
	async select_nama_movies (nama_movies){
		let result = await conn.query("select * from movies where nama_movies=? and status = 'A'",[nama_movies]);
		if (result.length > 0) return result[0].NAMA_MOVIES;
		else return false;
		
	}
	async insert_movies (nama_movies){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let movies = await conn.query("SELECT concat('M',lpad(substr(ID_MOVIES,2,4)+1,4,0)) ID_MOVIES FROM `movies` WHERE 1 ORDER BY ID_MOVIES DESC LIMIT 1");
		let id_movies = movies[0].ID_MOVIES;
		
		let result = "";
		try{
			result = await conn.query('insert into movies values (?,?,?)',[id_movies,nama_movies,'A']);
		} catch(error){
			console.log(error);
            return {status:400,result:error.sqlMessage};
        }
		
		if(result.affectedRows > 0) return {status:200,result:{id_movies:id_movies,nama_movies:nama_movies}};
	}
	async update_movies(id_movies,nama_movies){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let result = "";
		try{
			result = await conn.query('UPDATE movies SET NAMA_MOVIES=? WHERE ID_MOVIES=?',[nama_movies,id_movies]);
		} catch(error){
			console.log(error);
            return {status:400,result:error.sqlMessage};
		}
		
		if(result.affectedRows > 0) return {status:200,result:{id_movies:id_movies,nama_movies:nama_movies}};
		else return {status:400,result:"No Data Found"};
	}
	async delete_movies(id_movie){
		let getConnection = await modeluser.getConnection();
		if (modeluser.connectionError(getConnection.status)) return getConnection;
		
		let movies = await this.select_movies(id_movie,'%%');
		let nama_movie = '';
		
		if (movies.length > 0) nama_movie = movies[0].NAMA_MOVIES;
		
		let result = "";
		try{
			result = await conn.query("UPDATE movies SET status='X' WHERE id_movies = ?",[id_movie]);
		} catch(error){
			console.log(error);
            return {status:400,result:error.sqlMessage};
		}
		
		if(result.affectedRows > 0 && movies.length > 0) return {status:200,result:{id_movies:id_movie,nama_movies:nama_movie}};
		else return {status:400,result:"No Data Found"};
	}
}

module.exports = new ModelMovies();