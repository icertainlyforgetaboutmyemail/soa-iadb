/* Load Database */
let db = require('./db');
/* Load Bcrypt untuk enkripsi */
const bcrypt = require("bcrypt");

class ModelUser{
	async checkAccount(email_user){
		let getConnection = await this.getConnection();
        if (this.connectionError(getConnection.status)) return getConnection;
		
		/* Query user (email_user,nama_user,password_user) */
        let query = await db.query(
            "select * from user where email_user = ?",
            [email_user]
        );
		
		if(query.length > 0){
			return {status:200,result:query};
		}
		
		else {
			return {status:401,result:"Unathorized"};
		}
	}
    /* Untuk pengecekan select akun dengan login */
    async login(email_user,password_user){
        /* Cek Get Connection */
        let getConnection = await this.getConnection();
        if (this.connectionError(getConnection.status)) return getConnection;

        /* Query user (email_user,nama_user,password_user) */
        let query = await db.query(
            "select * from user where email_user = ?",
            [email_user]
        );

        /* Bila ada data user */
        if(query.length > 0){
            /* Cek password yang menggunakan Enkripsi Bcrypt */
            let check_password = await bcrypt.compare(password_user,query[0].PASSWORD_USER);
            if (check_password) return {status:200,result:query};
        }

        /* Bila tidak ada atau password salah. Kirim status 401 (Unathorized) */
        return {status:400,result:"Bad Request"};
    }
    
    /* Untuk pengecekan insert akun dengan register */
    async register(email_user,nama_user,password_user){
        /* Cek Get Connection */
        let getConnection = await this.getConnection();
        if (this.connectionError(getConnection.status)) return getConnection;

        try{
            /* Query insert user */
            let query = await db.query(
                "insert into user values (?,?,?,'U')",
                [email_user,nama_user,password_user]
            );
        } catch(error){
            /* Bila ada insert SQL Error kirim status 400 (Bad Request) */
            return {status:400,result:error.sqlMessage};
        }

        /* Bila data sukses tersimpan kirim status 201 (Created) */
        return {status:201,result:{email_user:email_user,nama_user:nama_user,password_user:password_user,level:"U"}};
    }

    /* Untuk pengecekan Koneksi Database */
    async getConnection() {
        console.log('Error Connection Message : ');
        try{
            /* Cek koneksi */
            await db.getConnection();
        }catch(error){
            /* Bila Error kirim status 500 (Internal Server Error) */
            return {status:500,result:"Error database connection"};            
        }
        /* Bila Sukses kirim status 200 (Ok) */
        console.log('NULL message');
        return {status:200,result:"Database connection fix"};            
    }

    /* Untuk pengecekan Error */
    connectionError(status){
        return status!=200; // Bila status tidak ok
    }
}

module.exports=new ModelUser();