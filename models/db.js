/* Load untuk mysql */
let mysql = require("mysql");
/* Load supaya tidak berjalan secara callback hail */
let util = require("util");

/* Supaya tidak open connection bolak balik */
let pool = mysql.createPool({
	connectionLimit:10,
	host:"localhost",
	database:"iadb",
	user:"root",
	password:""
});

/* Get Connection supaya berjalan secara async dan await tidak secara callback hail */
pool.getConnection = util.promisify(pool.getConnection);
/* Pool.Query supaya berjalan secara async dan await tidak secara callback hail */
pool.query = util.promisify(pool.query);
module.exports=pool;