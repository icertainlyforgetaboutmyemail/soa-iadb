const express= require("express");
const router = express.Router();
const db = require('../models/db');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
let yup = require("yup");

let ControllerUser = require("../controllers/ControllerUser");
let ControllerActor = require("../controllers/ControllerActor");

router.get('/',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"U");},
	(req,res,next)=>{ControllerActor.listActordgPencarian(req,res,next)}
)

router.get('/:ID_AKTOR',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"U");},
	(req,res,next)=>{ControllerActor.listActorDgID_AKTOR(req,res,next)}
)

router.post('/',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	async function (req,res,next) {ControllerActor.insertActor(req,res,next);}
);

router.post('/cast/:id_aktor',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	async function (req,res,next) {ControllerActor.insertCast(req,res,next);}
);

router.put('/:id_aktor',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	async function (req,res,next) {ControllerActor.updateActor(req,res,next);}
);

router.delete('/:id_aktor',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	async function (req,res,next) {ControllerActor.deleteActor(req,res,next);}
);

router.delete('/cast/:id_aktor/:id_movie',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	async function (req,res,next) {ControllerActor.deleteCast(req,res,next);}
);

module.exports = router;