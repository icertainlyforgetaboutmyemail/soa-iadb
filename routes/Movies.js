const express = require("express");
const router = express.Router();
const db = require('../models/db');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
let yup = require("yup");
let request = require('request');

let ControllerUser = require("../controllers/ControllerUser");
let ControllerMovies = require("../controllers/ControllerMovies");

router.get('/',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"U");},
	(req,res,next)=>{ControllerMovies.listMovies(req,res,next);}
);
router.post('/',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	(req,res,next)=>{ControllerMovies.insertMovies(req,res,next);}
);
router.put('/:id_movies',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	(req,res,next)=>{ControllerMovies.updateMovies(req,res,next);}
);
router.delete('/:id_movies',
	(req,res,next)=>{ControllerUser.auth(req,res,next,"A");},
	(req,res,next)=>{ControllerMovies.deleteMovies(req,res,next);}
);

module.exports = router;