const express= require("express");
const router = express.Router();

/* Load Controller */
const controllerUser = require('../controllers/ControllerUser');

router.post('/register',async (req,res,next)=>{controllerUser.register(req,res,next)});
router.post('/login', async (req,res,next)=>{controllerUser.login(req,res,next)});

module.exports = router;